/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bh08.quiz.daos;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.bh08.quiz.data.MysqlConnection;
import com.bh08.quiz.exceptions.DataAccessException;
import com.bh08.quiz.model.Answer;
import com.bh08.quiz.model.HallOfFame;
import com.bh08.quiz.model.Question;
import com.bh08.quiz.model.UserResult;

/**
 *
 * @author Majka
 */
public class ResultDAO {
	MysqlConnection mysqlConnection;
	private Statement statement;

	public ResultDAO() {
		init();
	}

	public void init() {
		mysqlConnection = new MysqlConnection();
	}

	public HallOfFame getHallOfFame() {
		HallOfFame hallOfFame= new HallOfFame();
		try (Connection con = mysqlConnection.getConnection()) {
			String sql="SELECT username, SUM(points) as points "
					+ "FROM UserPoints "
					+ "GROUP BY username "
					+ "ORDER BY SUM(points) "
					+ "LIMIT 3";
			statement = con.createStatement();
			ResultSet rs = statement.executeQuery(sql);
			int i=0;
			while (rs.next()) {
				String username = rs.getString("username");
				Integer points = rs.getInt("points");
				i++;
				switch(i) {
					case 1:
						hallOfFame.setThird(new UserResult(username, points));
						break;						
					case 2:
						hallOfFame.setSecond(new UserResult(username, points));
						break;						
					case 3:
						hallOfFame.setFirst(new UserResult(username, points));
						break;
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return hallOfFame;
	}
}
