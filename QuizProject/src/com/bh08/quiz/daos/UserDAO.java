package com.bh08.quiz.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.commons.codec.digest.DigestUtils;

import com.bh08.quiz.data.MysqlConnection;
import com.bh08.quiz.exceptions.AuthenticationException;
import com.bh08.quiz.exceptions.DataAccessException;
import com.bh08.quiz.model.Question;
import com.bh08.quiz.model.User;

public class UserDAO {
	MysqlConnection mysqlConnection;
	private Statement statement;
	private final String GET_PASSWORD_BY_USERNAME = "SELECT password FROM Users WHERE username LIKE ?";
	private final String CHECK_IF_USERNAME_EXISTS = "SELECT username FROM Users WHERE username LIKE ?";
	private final String INSERT_NEW_USER="INSERT INTO Users (username, password) VALUES (?,?)";
	
	public UserDAO() {
		init();
	}

	public void init() {
		mysqlConnection = new MysqlConnection();
	}
		

	public Optional<User> getUserByUserNameAndPassword(String username, String password) throws DataAccessException {
		
		String encryptedPassword = DigestUtils.sha1Hex(password);
		try (Connection con = mysqlConnection.getConnection();) {
			PreparedStatement preparedStatement = con.prepareStatement(GET_PASSWORD_BY_USERNAME);
			preparedStatement.setString(1, username);
			ResultSet rs = preparedStatement.executeQuery();
			while (rs.next()) {
				String dbEncryptedPassword = rs.getString("password");
				if(dbEncryptedPassword.equals(encryptedPassword)) {
					return Optional.of(new User(username));					
				}
				else {
					return Optional.empty();
				}

			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DataAccessException(e);
		}

		return Optional.empty();
		
	}

	private Optional<User> getUserByUserName(String username) throws DataAccessException{
		try (Connection con = mysqlConnection.getConnection();) {
			PreparedStatement preparedStatement = con.prepareStatement(CHECK_IF_USERNAME_EXISTS);
			preparedStatement.setString(1, username);
			ResultSet rs = preparedStatement.executeQuery();
			while (rs.next()) {
				return Optional.of(new User(username));					
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DataAccessException(e);
		}

		return Optional.empty();
		
	}
	
	public void insertUserByUserNameAndPassword(String username, String password) throws DataAccessException, AuthenticationException {
		if(getUserByUserName(username).isPresent()) {
			throw new AuthenticationException("The username is taken.");
		}
		else {
			String encryptedPassword = DigestUtils.sha1Hex(password);
			try (Connection con = mysqlConnection.getConnection();) {
				PreparedStatement preparedStatement = con.prepareStatement(INSERT_NEW_USER);
				preparedStatement.setString(1, username);
				preparedStatement.setString(2, encryptedPassword);
				preparedStatement.executeUpdate();
			} catch (SQLException e) {
				e.printStackTrace();
				throw new DataAccessException(e);
			}
		}
	}
	
}
