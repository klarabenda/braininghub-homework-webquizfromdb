/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bh08.quiz.daos;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.bh08.quiz.data.MysqlConnection;
import com.bh08.quiz.exceptions.DataAccessException;
import com.bh08.quiz.model.Answer;
import com.bh08.quiz.model.Question;

/**
 *
 * @author Majka
 */
public class QuestionDAO {
	MysqlConnection mysqlConnection;
	private Statement statement;

	public QuestionDAO() {
		init();
	}

	public void init() {
		mysqlConnection = new MysqlConnection();
	}

	public List<Question> getRandomQuestions(int questionCount) {
		List<Question> quizQuestions = new ArrayList<>();
		try (Connection con = mysqlConnection.getConnection()) {
			String sql="SELECT questionId, question "
					+ "FROM questions "
					+ "ORDER BY RAND() "
					+ "LIMIT "+questionCount ;
			statement = con.createStatement();
			ResultSet questionResultSet = statement.executeQuery(sql);
			int positionId = 1;
			while (questionResultSet.next()) {
				int questionId = questionResultSet.getInt("questionId");
				String questionText = questionResultSet.getString("question");
				Question question = new Question(questionId, positionId, questionText);
				quizQuestions.add(question);
				positionId++;
				//System.out.println(question);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return quizQuestions;

	}


	public List<Question> getAnswers(List<Question> quizQuestions) {
		String questionList = "";
		for (Question q : quizQuestions) {
			questionList = questionList + q.getQuestionId() + ", ";
		}
		//System.out.println(questionList);
		questionList = questionList.substring(0, questionList.length() - 2);
		//System.out.println(questionList);
		try (Connection con = mysqlConnection.getConnection()) {
			Statement st = con.createStatement();

			String sql = "SELECT q.questionId as questionid, a.answerId as answerid, a.answer as answer "
					+ "FROM questions q " + "JOIN answerstoquestion r " + "ON q.questionId = r.questionId "
					+ "JOIN answers a " + "ON a.answerId = r.answerId " + "WHERE q.questionId IN (" + questionList
					+ ")";

			ResultSet answersResultSet = st.executeQuery(sql);
			while (answersResultSet.next()) {
				int questionId = answersResultSet.getInt("questionid");
				int answerId = answersResultSet.getInt("answerid");
				String answer = answersResultSet.getString("answer");
				for (Question q : quizQuestions) {
					if (q.getQuestionId().equals(questionId)) {
						q.addAnswer(new Answer(answerId, q.getAnswerCount() + 1, answer));
					}
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return quizQuestions;
	}

	public int getCorrectAnswerCount(List<String> answerIds) {
		int correctAnswerCount=0;
		String answerList = "";
		for (String answerId: answerIds) {
			answerList = answerList + answerId + ", ";
		}
		answerList = answerList.substring(0, answerList.length() - 2);
		//System.out.println("Answers from quiz: " + answerList);
		
		try (Connection con = mysqlConnection.getConnection()) {
			Statement st = con.createStatement();

			String sql = "SELECT answerId "
					+ "FROM AnswersToQuestion "
					+ "WHERE answerId IN "
					+ "(" + answerList + ")"
					+ "AND isCorrect=1";

			ResultSet answersResultSet = st.executeQuery(sql);
			while (answersResultSet.next()) {
				correctAnswerCount++;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		return correctAnswerCount;
	}
	public void insertUserPoints(String username, Integer points) throws DataAccessException {
		try (Connection con = mysqlConnection.getConnection();) {
			String sql="INSERT INTO UserPoints (username, points) VALUES ('"+ username+ "',"+points+")";
			statement = con.createStatement();
			statement.executeUpdate(sql);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DataAccessException(e);
		}
		
	}	
	
}
