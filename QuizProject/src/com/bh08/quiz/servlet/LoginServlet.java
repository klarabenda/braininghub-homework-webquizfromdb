package com.bh08.quiz.servlet;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.tomcat.websocket.AuthenticationException;

import com.bh08.quiz.exceptions.DataAccessException;
import com.bh08.quiz.exceptions.EmptyFieldsException;
import com.bh08.quiz.model.User;
import com.bh08.quiz.service.UserService;
import com.bh08.quiz.text.Messages;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/login")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static UserService userService;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
		userService = UserService.getInstance();

        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		System.out.println(username);
		if (request.getParameter("submit-button") != null){
			try {
				User user = userService.login(username, password);
				HttpSession session = request.getSession(true);
				session.setAttribute("user", user);
				session.setAttribute("loginTime", new Date());
				
				System.out.println(username + " has logged in.");

				request.getRequestDispatcher("/quiz").forward(request, response);
				
			} catch (com.bh08.quiz.exceptions.AuthenticationException | DataAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				request.setAttribute("error", Messages.ERROR_NOT_LOGGED_IN);
				request.getRequestDispatcher("/index.jsp").forward(request, response);
			}
		 }
		else if (request.getParameter("register-button") != null){
			try {
				userService.register(username, password);
				System.out.println(username + " has registered.");
				request.getRequestDispatcher("/index.jsp").forward(request, response);
				
			} catch (com.bh08.quiz.exceptions.AuthenticationException | DataAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				request.setAttribute("error", Messages.USER_NOT_SAVED);
				request.getRequestDispatcher("/index.jsp").forward(request, response);
			} catch (EmptyFieldsException e) {
				request.setAttribute("error", e.getMessage());
				request.getRequestDispatcher("/index.jsp").forward(request, response);
			}
			
		}
		else {
			System.out.println("Unidentified button");

		}
		
		

	}

}
