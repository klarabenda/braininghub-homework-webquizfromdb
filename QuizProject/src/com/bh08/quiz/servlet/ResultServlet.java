package com.bh08.quiz.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bh08.quiz.exceptions.DataAccessException;
import com.bh08.quiz.model.User;
import com.bh08.quiz.service.QuizService;
import com.bh08.quiz.text.Messages;

/**
 * Servlet implementation class ResultServlet
 */
@WebServlet("/result")
public class ResultServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private static QuizService quizService;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ResultServlet() {
        super();
        quizService = QuizService.getInstance();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(false);
		if (session != null) {
			session.setAttribute("quiz", null);
			User user = (User) session.getAttribute("user");
			if (user != null) {

				doPointCounts(request, user);
				doHallOfFame(request);
				request.getRequestDispatcher("/result.jsp").forward(request, response);

			} else {
				request.setAttribute("error", Messages.ERROR_NOT_LOGGED_IN);
				request.getRequestDispatcher("/index.jsp").forward(request, response);
			}
			
		} else {
			request.setAttribute("error", Messages.ERROR_NOT_LOGGED_IN);
			request.getRequestDispatcher("/index.jsp").forward(request, response);
		}
		

		
		
	}

	
	private void doPointCounts(HttpServletRequest request, User user) {
		Integer questionCount = Integer.parseInt(request.getParameter("questionCount")) ;
		List<String> submittedAnswerIds = getSubmittedAnswers(request, questionCount); 
		Integer submittedAnswerCount=submittedAnswerIds.size();
		Integer correctAnswerCount=quizService.getCorrectAnswerCount(submittedAnswerIds);

		saveUserPoints(request, user, correctAnswerCount);
		
		request.setAttribute("username", user.getUserName());
		request.setAttribute("questionCount", questionCount);
		request.setAttribute("correctAnswerCount", correctAnswerCount);
		request.setAttribute("submittedAnswerCount", submittedAnswerCount);
	}
	
	private List<String> getSubmittedAnswers(HttpServletRequest request, Integer questionCount){
		List<String> submittedAnswerIds = new ArrayList<>();
		for(int i=1; i<=questionCount; i++) {
			String answerId = request.getParameter(Integer.toString(i));
			if(answerId!=null) {
				submittedAnswerIds.add(answerId);
			}
		}
		return submittedAnswerIds;

	}
	
	private void saveUserPoints(HttpServletRequest request,  User user, Integer correctAnswerCount) {
		try {
			quizService.saveUserPoints(user, correctAnswerCount);
		} catch (DataAccessException e) {
			request.setAttribute("error", Messages.QUIZ_POINTS_NOT_SAVED);
			e.printStackTrace();
		}
		
	}
	
	private void doHallOfFame(HttpServletRequest request) {
		request.setAttribute("hallOfFame", quizService.getHallOfFame());
	}
	

}
