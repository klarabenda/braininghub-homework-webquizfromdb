package com.bh08.quiz.servlet;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bh08.quiz.model.User;
import com.bh08.quiz.text.Messages;
import com.bh08.quiz.daos.QuestionDAO;
import com.bh08.quiz.daos.ResultDAO;
import com.bh08.quiz.model.Question;
import com.bh08.quiz.model.Quiz;
import com.bh08.quiz.service.QuizService;


/**
 * Servlet implementation class Quiz
 */
@WebServlet("/quiz")
public class QuizServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private static QuizService quizService;
    private static final int QUESTION_COUNT=5;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public QuizServlet() {
        super();
        quizService = QuizService.getInstance();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(false);
		if (session != null) {
			User user = (User) session.getAttribute("user");
			Date date = (Date) session.getAttribute("loginTime");
			Quiz quiz = (Quiz) session.getAttribute("quiz");
			if (user != null) {
				if(quiz==null) {
					session.setAttribute("quiz", quizService.getQuiz(QUESTION_COUNT));
					//request.setAttribute("quiz", quizService.getQuiz(QUESTION_COUNT));
				}
				request.getRequestDispatcher("/quiz.jsp").forward(request, response);	
			} else {
				request.setAttribute("error", Messages.ERROR_NOT_LOGGED_IN);
				request.getRequestDispatcher("/index.jsp").forward(request, response);
			}
			
		} else {
			request.setAttribute("error", Messages.ERROR_NOT_LOGGED_IN);
			request.getRequestDispatcher("/index.jsp").forward(request, response);
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
	
}
