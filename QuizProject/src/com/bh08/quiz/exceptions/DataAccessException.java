package com.bh08.quiz.exceptions;

public class DataAccessException extends Exception {

	public DataAccessException(Throwable e) {
		super("A data source exception has occured.", e);
	}
	
	
	
}
