package com.bh08.quiz.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Question implements Serializable {
	Integer questionId;
	Integer positionId;
	String question;
	List<Answer> answers;
	
	public Question(Integer questionId, Integer positionId, String question){
		this.questionId=questionId;
		this.positionId=positionId;
		this.question=question;
		this.answers = new ArrayList<>();
	}

	public Integer getQuestionId() {
		return questionId;
	}

	public void setQuestionId(Integer questionId) {
		this.questionId = questionId;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public List<Answer> getAnswers() {
		return answers;
	}

	public void setAnswers(List<Answer> answers) {
		this.answers = answers;
	}
	
	public int getAnswerCount() {
		return answers.size();
	}
	
	public void addAnswer(Answer answer) {
		this.answers.add(answer);
		
	}

	public Integer getPositionId() {
		return positionId;
	}

	public void setPositionId(Integer positionId) {
		this.positionId = positionId;
	}

	@Override
	public String toString() {
		return "Question [questionId=" + questionId + ", positionId=" + positionId + ", question=" + question
				+ ", answers=" + answers + "]";
	}
	
}

