package com.bh08.quiz.model;

import java.io.Serializable;

public class Answer implements Serializable {
	Integer answerId;
	Integer positionId;
	String answer;	
	
	public Answer(Integer answerId, Integer positionId, String answer){
		this.positionId=positionId;
		this.answerId=answerId;
		this.answer=answer;
	}

	public Integer getAnswerId() {
		return answerId;
	}

	public void setAnswerId(Integer answerId) {
		this.answerId = answerId;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public Integer getPositionId() {
		return positionId;
	}

	public void setPositionId(Integer positionId) {
		this.positionId = positionId;
	}

	@Override
	public String toString() {
		return "Answer [answerId=" + answerId + ", positionId=" + positionId + ", answer=" + answer + "]";
	}



}