package com.bh08.quiz.model;

import java.io.Serializable;

public class HallOfFame implements Serializable {
	private UserResult first;
	private UserResult second;
	private UserResult third;
	
	public HallOfFame() {
		init();
	}
	
	private void init() {
		this.first=new UserResult("Not available", 0);
		this.second=new UserResult("Not available", 0);
		this.third=new UserResult("Not available", 0);
	}

	public UserResult getFirst() {
		return first;
	}

	public UserResult getSecond() {
		return second;
	}

	public UserResult getThird() {
		return third;
	}

	public void setFirst(UserResult first) {
		this.first = first;
	}

	public void setSecond(UserResult second) {
		this.second = second;
	}

	public void setThird(UserResult third) {
		this.third = third;
	}
	
}
