package com.bh08.quiz.model;

public class UserResult {
	private String userName;
	private Integer points;
	
	public UserResult(String userName, Integer points) {
		super();
		this.userName = userName;
		this.points = points;
	}

	public String getUserName() {
		return userName;
	}

	public Integer getPoints() {
		return points;
	}

	@Override
	public String toString() {
		return "UserResult [userName=" + userName + ", points=" + points + "]";
	}
	
	

}
