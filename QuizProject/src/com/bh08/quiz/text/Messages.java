package com.bh08.quiz.text;

public interface Messages {

	public String ERROR_NOT_LOGGED_IN = "You are not logged in!";
	public String MSG_LOGOUT_SUCCESS = "Logout successful";
	public String QUIZ_POINTS_NOT_SAVED = "Quiz points could not be saved.";
	public String USER_NOT_SAVED = "This username is already in the database.";
	
}
