package com.bh08.quiz.service;

import java.util.Optional;

import com.bh08.quiz.daos.UserDAO;
import com.bh08.quiz.exceptions.AuthenticationException;
import com.bh08.quiz.exceptions.DataAccessException;
import com.bh08.quiz.exceptions.EmptyFieldsException;
import com.bh08.quiz.model.User;

public class UserService {
	
	private static UserService instance;
	private UserDAO dao = new UserDAO();
	
	private UserService() {
		
	}
	
	public static UserService getInstance() {
		if (instance == null) {
			instance = new UserService();
		}
		return instance;
	}


	public User login(String username, String password) throws AuthenticationException, DataAccessException {
		Optional<User> result = dao.getUserByUserNameAndPassword(username, password);
		if (result.isPresent()) {
			return result.get();
		} else {
			throw new AuthenticationException("Login failed: username or password invalid.");
		}
		
	}
	
	public void register(String username, String password) throws DataAccessException, AuthenticationException, EmptyFieldsException {
		if("".equals(username)) {
			throw new EmptyFieldsException("Username is empty");
		} else if("".equals(password)) {
			throw new EmptyFieldsException("Password is empty");
		} else {
			dao.insertUserByUserNameAndPassword(username, password);
		}
		
	}

}
