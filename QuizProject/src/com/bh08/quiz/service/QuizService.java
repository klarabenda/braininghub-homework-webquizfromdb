package com.bh08.quiz.service;

import java.util.List;


import com.bh08.quiz.daos.QuestionDAO;
import com.bh08.quiz.daos.ResultDAO;
import com.bh08.quiz.exceptions.DataAccessException;
import com.bh08.quiz.model.HallOfFame;
import com.bh08.quiz.model.Question;
import com.bh08.quiz.model.Quiz;
import com.bh08.quiz.model.User;

public class QuizService {
	private static QuizService instance;
    private QuestionDAO questionDao;
    private ResultDAO resultDao;

    private QuizService() {
    	questionDao=new QuestionDAO();
    	resultDao= new ResultDAO();
    }
    
	public static QuizService getInstance() {
		if (instance == null) {
			instance = new QuizService();
		}
		return instance;
	}
    
    public Quiz getQuiz(int questionCount) {
    	List<Question> questionList=questionDao.getRandomQuestions(questionCount);
    	questionList=questionDao.getAnswers(questionList);
    	return new Quiz(questionList);
    }
    
    public int getCorrectAnswerCount(List<String> answerIds) {
    	return questionDao.getCorrectAnswerCount(answerIds);
    }
    
    public void saveUserPoints(User user, Integer points) throws DataAccessException {
    	questionDao.insertUserPoints(user.getUserName(), points);
    }
    
    public HallOfFame getHallOfFame() {
    	return resultDao.getHallOfFame();
    }

}
