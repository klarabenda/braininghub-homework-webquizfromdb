<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@page import="com.bh08.quiz.model.*"%>
<%	Quiz quiz = (Quiz) session.getAttribute("quiz"); %>
<!doctype html>
<html lang="en">

<head>
<meta charset="UTF-8">
<title>Quiz</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
	integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
	crossorigin="anonymous" />
</head>

<body>
	<div class="card">
		<div class="card-body">
			<h5 class="card-title">Java Quiz</h5>
			<p class="card-text">Make your answers and submit to check your knowledge. </p>
			<form method="POST" action="/QuizProject/result">


<%
	for (Question question : quiz.getQuestions()) {
		int questionPosition = question.getPositionId();
		out.println("<div class='card' style='width: 30rem;'>");
		out.println("<div class='card-header'>");
		out.println(questionPosition + ". " + question.getQuestion());
		out.println("</div>");
		for(Answer answer : question.getAnswers()){
			out.println("<ul class='list-group list-group-flush'>");
	        out.println("<li class='list-group-item'><div class='custom-control custom-radio'>");
	        out.println("<input type='radio' id='" + answer.getAnswerId() + "' value='" + answer.getAnswerId() + "' name='" + questionPosition+ "' class='custom-control-input'>");
	        out.println("<label class='custom-control-label' for='" + answer.getAnswerId() + "'>");
	        out.println(answer.getAnswer());
	        out.println("</label></div></li>");
		}
		out.println("</ul></div>");
	}
	out.println("<input type='hidden' id='questionCount' name='questionCount' value='"+quiz.getQuestions().size()+"'>");	

%>
				<br /> <input type="submit" value="Submit" />
			</form>
		</div>
	</div>


</body>

</html>