<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@page import="com.bh08.quiz.model.HallOfFame"%>
<%@page import="com.bh08.quiz.model.UserResult"%>
<%	
Integer correctAnswerCount = (Integer) request.getAttribute("correctAnswerCount"); 
Integer submittedAnswerCount = (Integer) request.getAttribute("submittedAnswerCount"); 
Integer questionCount = (Integer) request.getAttribute("questionCount"); 
String username = (String) request.getAttribute("username"); 
String errorMessage = (String) request.getAttribute("error"); 
HallOfFame hallOfFame = (HallOfFame) request.getAttribute("hallOfFame");
%>
<!DOCTYPE html>
<html>
<head>
<title>Quiz Results</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
	integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
	crossorigin="anonymous" />

</head>
<body>
<h3>
  Quiz Results
  <% 
  if(username != null){
	  out.println("<small class='text-muted'>for "); 
	  out.println(username);
	  out.println("</small>");
  }
  %>
</h3>
<p class="lead">
  <% 
  if(username != null){
	  out.println("You submitted "); 
		 out.println(submittedAnswerCount);
	     out.println(" answers out of ");
	     out.println(questionCount);
	     out.println(" questions in total.<br/>");
	     out.println("Out of ");
		 out.println(submittedAnswerCount);
	     out.println(" you have ");
	     out.println(correctAnswerCount);
	     out.println(" correct answers.");
  }
  if(errorMessage !=null){
    out.println("<br/><br/>" + errorMessage);	 
	}
  %>
</p>
<h3>
  Hall of Fame
</h3>
<% 
out.println("Top score:<br/>");
out.println(hallOfFame.getFirst().getUserName());
out.println(": ");
out.println(hallOfFame.getFirst().getPoints());
out.println(" points.<br/><br/>");

out.println("Second:<br/>");
out.println(hallOfFame.getSecond().getUserName());
out.println(": ");
out.println(hallOfFame.getSecond().getPoints());
out.println(" points.<br/><br/>");

out.println("Third:<br/>");
out.println(hallOfFame.getThird().getUserName());
out.println(": ");
out.println(hallOfFame.getThird().getPoints());
out.println(" points.<br/><br/>");
%>

</body>
</html>