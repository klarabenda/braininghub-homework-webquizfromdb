<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%	
String errorMessage = (String) request.getAttribute("error"); 
%>

<!DOCTYPE html>
<html>
<head>
<title>Quiz Login</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
	integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
	crossorigin="anonymous" />

</head>
<body>
<div class="jumbotron jumbotron-fluid" style='width: 40rem;'>
  <div class="container">
    <h1 class="display-4">Welcome in the Java Quiz App</h1>
    <%if(errorMessage !=null){
    	out.println("<p class='lead'>" +errorMessage+ "</p>");
    } else {
		out.println("<p class='lead'>Please login to access the tests.</p>");
    }
    
    %>
    
  </div>
</div>
<form method="POST" action="/QuizProject/login">
  <div class="quiz-login">
    <label for="username">Username</label>
    <input type="text" class="form-control" id="username" name="username" placeholder="Enter username" style='width: 30rem;'>
  </div>
  <div class="quiz-login">
    <label for="password">Password</label>
    <input type="password" class="form-control" id="password" name="password" placeholder="Password" style='width: 30rem;'>
  </div>
  <br/>
  <button type="submit" class="btn btn-primary" name="submit-button" value="ok">Submit</button>   <button type="submit" class="btn btn-primary" name="register-button" value="ok">Register</button>
</form>
</body>
</html>